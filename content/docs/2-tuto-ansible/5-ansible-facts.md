---
weight: 5
bookToC: false
title: "Slides facts Ansible"
---

# Slides facts Ansible

Cours sur les "facts" d'Ansible.

Nécessaire pour **l'étape 3** du projet.

## [Lien vers les slides](/slides/3-facts-templates/3-facts-templates.pdf)
