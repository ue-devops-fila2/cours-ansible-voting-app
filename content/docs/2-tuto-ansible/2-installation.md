---
weight: 2
title: "Installation d'Ansible"
---

# Installation d'Ansible

## Pour les machines Windows : installation de WSL

Le contrôleur Ansible ne fonctionne que sur Linux et macOS. Pour les machines Windows, nous ferons le TP sur WSL:

Cliquer sur **Windows** -> Rechercher **Activer ou désactiver des fonctionnalités Windows**
- Sélectionner **Sous-système Windows pour Linux**

Ouvrir **Powershell** en mode administrateur:
- Installer la distribution Ubuntu: ```wsl --install -d Ubuntu```
- Rentrer un nom d'utilisateur et un mot de passe

## Installation de pip
Vérifier si pip est installé : ```python3 -m pip -V```

Sinon, installer pip :
  ```
  sudo apt install python3-pip
  ```

## Installation d'Ansible
Installer Ansible via pip: ```python3 -m pip install ansible```

Vérifier que Ansible est bien installé et accessible: ```ansible --version```

