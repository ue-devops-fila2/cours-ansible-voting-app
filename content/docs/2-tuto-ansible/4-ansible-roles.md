---
weight: 4
bookToC: false
title: "Slides rôles Ansible"
---

# Slides rôles Ansible

Cours sur les rôles Ansible.

Nécessaire pour **l'étape 1** du projet.

## [Lien vers les slides](/slides/2-reusable-ansible/2-reusable-ansible.pdf)
