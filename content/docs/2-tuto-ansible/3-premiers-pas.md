---
weight: 3
bookToC: false
title: "Premiers pas avec Ansible"
---

# Premiers pas avec Ansible

## Squelette de projet

Voilà le dépôt git à forker et clone : <https://gitlab.imt-atlantique.fr/ue-devops-fila2/tp-ansible-voting-app>

Vous pouvez réutiliser un fork de ce dépôt tout au long du TP pour le
projet et le rendu.

## Créer une VM sur GCP

Nous travaillerons sur une VM GCP. Commencez par créer une instance via le tableau de bord.
  * Choisissez bien une Région et une Zone en Europe, par exemple `europe-west9` et `europe-west9-b`, identique à celles de votre cluster Kubernetes.
  * Vérifiez que vous pouvez vous connecter en SSH (changez le nom de l'instance):
    ```
    gcloud compute ssh instance-20240430-95bdf
    ```

## Remplir l'inventaire

Remplissez `inventories/gcp.yaml` avec l'IP de votre VM.  Adaptez le
chemin vers la clé SSH.

## Lancer son premier playbook

Lancer le premier playbook en mode test (sans vraiment exécuter les actions) :

    ansible-playbook --check demo-1-simple.yml

Si ça vous semble correct, vous pouvez lancer le playbook "pour de vrai" :

    ansible-playbook demo-1-simple.yml

Bravo, vous venez de faire votre premier déploiement avec Ansible !

## Autres utilisations de ansible-playbook

Si besoin, vous pouvez préciser un inventaire spécifique :

    ansible-playbook -i inventories/production.yaml demo-1-simple.yml

C'est très utile lorsqu'on dispose d'une deuxième infra de
"pré-production" ou "qualification" : on peut alors tester complètement
notre déploiement sans casser la production.

Pour le TP, nous avons pré-configuré Ansible pour qu'il utilise l'inventaire
`inventories/gcp.yaml` par défaut.

Une autre option très utilisée est de limiter l'exécution à une seule
machine :

    ansible-playbook -l vm1 demo-1-simple.yml
