---
weight: 1
title: "Load balancer"
---

# Load balancer (répartiteur de charge)

À la fin du projet principal, nous avons un cluster PostgreSQL redondant et
interconnecté avec notre application qui tourne dans Kubernetes.

![Architecture](/architecture.drawio.svg)

Cependant, il manque une pièce au puzzle : si le nœud primaire ou le nœud
réplique tombe en panne, une partie de notre application ne fonctionnera plus !

Nous souhaitons ajouter davantage de redondance au niveau de
l'application grâce à un répartiteur de charge pour la base de données.

**Cette partie du TP est facultative, elle permet d'avoir une meilleure note.**

## Ajout d'un load balancer pour les opérations SQL

Intégrez un load balancer spécifique à PostgreSQL qui se charge d'interpréter
les requêtes (lecture ou écriture) et les redirige vers le nœud *primary* ou
*standby* façon transparente.

Dans Kubernetes, modifiez vos manifestes de l'étape 4 pour n'utiliser qu'un
seul namespace (`default`), un seul `Service` et un seul `EndpointSlice` qui
pointe vers le load balancer créé.

Pour tester votre solution, vérifiez que l'application fonctionne toujours
en éteignant et rallumant chacune des VMs (vous pouvez simplement arrêter
le service PostgreSQL sur chaque VM).

