---
weight: 2
title: "Evaluation du projet"
---

# Evaluation

## Calendrier

- vendredi 3 mai : première séance le matin, autonomie l'après-midi
- lundi 6 mai : deuxième séance le matin
- lundi 13 mai : troisième séance le matin + quizz, autonomie l'après-midi
- jeudi 16 mai (soir) : rendu du code
- vendredi 17 mai (après-midi) : oraux sur l'ensemble de l'UE

## Rendu

Le rendu de l'ensemble de l'UE se fait sur Moodle : merci de rendre les liens vers les repository GitLab/GitHub publics de vos projets.

Attention à bien ajouter un README nous permettant de facilement exécuter vos déploiements avec notre machine et nos comptes GCP, la qualité du README sera prise en compte dans la notation.

Merci d'indiquer au début du fichier README votre binôme.

Pour la partie Ansible, nous attendons dans le dépôt git :

- un ou plusieurs inventaires
- au minimum deux playbooks : étape 1 + 3, étape 2
- possiblement des playbooks pour les parties optionnelles (étape 4, partie avancée load balancer)
- un README qui décrit rapidement l'organisation de vos rôles, quelles parties du TP vous avez fait, et surtout quels playbooks il faut lancer

**Le clustering de l'étape 4 ne fonctionnant pas comme prévu, cette partie devient optionnelle (détails dans le sujet).**

## Oral

Durée de l'oral par groupe de 2 : 15 minutes

Contenu de l'évaluation :
- 3 minutes : quelques slides explicatives de l'application et des
  différents déploiements effectués pour montrer que vous savez donner le
  contexte du travail (maximum 3 slides)
- 5 minutes : une démonstration de la version finale du projet en
  Kubernetes + Ansible (avec l'utilisation de vos propres images Docker
  dans votre Artifact Registry). Attention à bien préparer et automatiser
  pour que ça fonctionne le jour J sans faire 10.000 manips !
- 5 minutes : nous vous demanderons ensuite du détail sur 3 parties de
  code (Docker, Kubernetes, Ansible)

Les 2 personnes du groupe devront répondre aux questions à tour de
rôle. Il faut aussi se répartir la parole entre les slides et la démo.
