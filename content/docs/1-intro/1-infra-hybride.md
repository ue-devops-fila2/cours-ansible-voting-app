---
weight: 1
title: "Infrastructure hybride"
---

# Infrastructure hybride

Pour ce TP, nous allons considérer une **infrastructure hybride** :

- l'applicatif (l'application de vote) restera sur **Kubernetes**
- en revanche, la base de données sera installée et gérée directement sur
  des **machines dédiées** grâce à Ansible

Nous justifions ce besoin pour des questions de sécurité et de
performance.  D'une part, la base de données contient des données
personnelles critiques et nous voulons assurer un fort contrôle sur
l'accès à ces données.  Avoir ces données en interne permet davantage de
contrôle : nous pouvons contrôler les accès au serveur, mettre un pare-feu
devant, etc.  D'autre part, nous aimerions avoir des garanties sur les
performances de la base de données, ce qui est plus simple avec une
machine dédiée.

C'est un pattern relativement courant dans les infrastructures complexes :
tous les composants n'ont pas besoin des mêmes compromis en terme de
besoin, de coût, de fiabilité et de performance.

## Architecture du projet

Le but du projet est d'arriver à cette architecture finale :

![Architecture](/architecture.drawio.svg)

Nous procéderons par étapes jusqu'à cette étape finale.

## Rôle d'Ansible

Nous allons travailler avec des **machines dédiées**, c'est à dire une
machine nue qui ne vient qu'avec son système d'exploitation.  Ainsi, pas
de conteneurs Docker ou de contrôleur Kubernetes : nous disposons
seulement d'un accès SSH.

Comment **automatiser** la configuration de ces machines dédiées de façon
**déclarative** ?  C'est là où Ansible va nous aider.
