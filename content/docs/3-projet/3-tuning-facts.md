---
weight: 3
title: "Étape 3 : paramétrage système avancé"
---

# Étape 3 : paramétrage système avancé

**Pre-réquis : slides facts Ansible**

## Pourquoi faire du paramétrage système ?

Contrairement à Kubernetes, nous connaissons précisément l'environnement
d'exécution : CPU, mémoire, espace disque, configuration système Linux...
Cela nous permet d'adapter la configuration de notre base de données pour
améliorer ses performances ou sa fiabilité.  Nous pouvons également agir
directement sur la configuration du système.

## Modifier la configuration système

Nous souhaitons modifier le paramètre noyau [vm.vfs_cache_pressure](https://docs.kernel.org/admin-guide/sysctl/vm.html#vfs-cache-pressure)
et le définir à 90 (comme [gitlab](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/3969)).

Modifiez votre rôle pour qu'il modifie ce paramètre.  La configuration
doit être appliquée immédiatement, mais doit également être persistante
lors du reboot de la VM.

Pour vérifier la valeur du paramètre, vous pouvez utiliser la commande
suivante avant et après un reboot de la VM :

    sudo sysctl vm.vfs_cache_pressure

## Modifier la configuration logicielle

PostgreSQL dispose de nombreux paramètres pour régler sa performance et sa
consommation de ressources.  Nous allons nous intéresser au paramètre
`shared_buffers` qui détermine la quantité de mémoire allouée par PostgreSQL.

Consultez la documentation de PostgreSQL pour comprendre ce paramètre et
déterminez une valeur pertinente pour votre VM.  Vous pouvez utiliser les
commandes `htop` ou `free -m` pour connaître la quantité de mémoire de
votre VM.

Modifiez votre rôle pour qu'il définisse ce paramètre `shared_buffers`.
Dans un premier temps, vous pouvez définir la valeur "à la main".
N'oubliez pas de redémarrer le service.

### Automatisation grâce aux facts

L'approche précédente a un problème évident : que se passerait-il si on
redéployait PostgreSQL sur une VM avec plus de mémoire ?  Ou si nous
déployons simultanément sur plusieurs serveurs avec des quantités de
mémoire différentes ?  Il semble nécessaire d'automatiser le calcul de la
bonne valeur.

Pour nous aider, Ansible collecte automatiquement de nombreuses
informations sur les machines distantes : CPU, RAM, disque, version de
l'OS, adresses IP...  Il met à disposition ces informations dans des
variables spéciales appelées **facts**.

Visualisez les facts d'une de vos VM grâce à la commande suivante :

    ansible -m setup <votre VM>

Adaptez votre rôle pour qu'il calcule automatiquement la bonne valeur de
`shared_buffers` en fonction de la mémoire disponible.

**Question bonus :** prendre en compte le cas où le serveur a moins de 1
GB de RAM (dans ce cas, mettre 15%)
