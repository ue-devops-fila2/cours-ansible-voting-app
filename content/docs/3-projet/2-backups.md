---
weight: 2
title: "Étape 2 : mise en place de backups"
---

# Étape 2 : mise en place de backups

Nous souhaitons mettre en place un backup de nos données.  Pour cela, vous
allez vous appuyer sur Ansible pour réaliser ce backup via un "dump" et le
stocker en local sur votre laptop.  Ainsi, même si le serveur crashe et
perd ses données, nous serons capable de restaurer la base de données.

## Playbook

Écrire un playbook qui réalise un backup du contenu de la base de données
PostgreSQL et rapatrie ce backup sous forme d'un fichier sur votre
ordinateur.  Vous pouvez vous appuyer sur [la collection de modules
`postgresql`](https://docs.ansible.com/ansible/latest/collections/community/postgresql/index.html)
ou directement sur la commande `pg_dump`.

Ici, le code Ansible sera relativement simple, vous pouvez coder
directement dans le playbook sans passer par un rôle.

## Exercice bonus

**Point bonus :** si la base de données devient très volumineuse, le
serveur n'aura pas assez d'espace disque pour stocker le backup.  Faites
en sorte que votre playbook "streame" le contenu du backup jusqu'à votre
laptop sans stocker de copie intermédiaire.
