---
weight: 4
title: "Étape 4 : création d'un cluster postgresql (bonus)"
---

# Étape 4 : création d'un cluster postgresql

## Information importante pour le rendu et l'oral

**Attention, il semble que la réplication ne fonctionne pas correctement à la fin.
Vous pouvez considérer toute cette partie comme une partie bonus optionnelle.
Si vous travaillez dessus, assurez-vous au minimum que le noeud primaire reste fonctionnel et connectez
l'application uniquement au noeud primaire.  Indiquez dans le README l'état de votre travail.**

## Motivation

Pour augmenter la fiabilité du système, nous aimerions redonder notre
serveur PostgreSQL sur deux serveurs.  On appelle généralement ce genre de
configuration du **clustering**.

Il y a deux approches principales de clustering :

- **clustering primaire-standby :** un seul nœud accepte des écritures,
  c'est le nœud "primaire".  Il transmet un flux de réplication au
  deuxième nœud appelé "standby".  En cas de panne du nœud primaire, on
  peut alors reconfigurer le nœud standby pour qu'il devienne le nœud
  primaire, puisqu'il a déjà une copie des données.
- **clustering multimaster :** chaque nœud peut accepter des écritures et
  se coordonne avec l'autre nœud pour propager les écritures et éviter
  les conflits.

Nous allons nous limiter au premier cas qui est bien plus simple à mettre en place.

## Architecture cible

Dans cette étape, nous allons mettre en place l'architecture cible complète :

![Architecture](/architecture.drawio.svg)

## Mise en place du cluster PostgreSQL avec Ansible

Nous allons travailler sur deux VMs : vous pouvez supprimer la VM
précédente et en créer deux nouvelles, puis les intégrer à votre
inventaire.

Créez un nouveau rôle pour configurer la réplication.
**Attention**, cet unique rôle devra gérer les deux modes possibles "primary" ou "standby".

Conservez bien le playbook précédent fait à l'étape 1 et travaillez dans un
nouveau playbook.
Celui-ci devra utiliser le rôle d'installation créé à l'étape 1 ainsi que le nouveau pour la réplication.

Structure de playbook souhaitée :

    - name: Install postgresql on all hosts
      hosts:
        - PrimaryVM
        - StandbyVM
      roles: postgresql_installation

    - name: Setup replication on primary VM
      hosts: PrimaryVM
      roles: postgresql_replication
      vars:
        postgresql_mode: "primary"
        postgresql_standby_ip: "..."

    - name: Setup replication on standby VM
      hosts: StandbyVM
      roles: postgresql_replication
      vars:
        postgresql_mode: "standby"
        postgresql_primary_ip: "..."

## Création du rôle pour la réplication

Côté nœud *primary* :
  1. Créer un utilisateur PostgreSQL spécifique pour la réplication (e.g. `replicator`) avec le droit associé (`role_attr_flags: replication`).
  1. Accorder le droit de connexion à cet utilisateur via le module `postgresql_pg_hba`. Idem que dans le rôle de l'étape 1.
  1. Accorder l'accès au nœud *standby* via le module `postgresql_pg_hba`. Idem que le point précédent mais avec `databases: replication` et `address` l'IP de *standby*.
  1. Via le module `postgresql_set`, définir les valeurs suivantes (utilisez une boucle)
      * `wal_level` à `'replica'`
      * `max_wal_senders` à `'10'`
  1. Via le module `lineinfile`, définir `wal_keep_size` à '1GB' dans le fichier `/etc/postgresql/15/main/postgresql.conf` (ce paramètre ne peut pas être défini par `postgresql_set`)
  1. Redémarrer le service `postgresql`.

Côté nœud *standby*, idem que pour *primary* en adaptant le point 3, avec **en plus** :
  1. Via le module `postgresql_set`, définir les valeurs suivantes (utilisez une boucle)
      * `listen_addresses` à `'0.0.0.0'`
      * `hot_standby` à `'on'`
  1. En utilisant le module `template` en tant que root, créer un fichier `/tmp/.pgpass` avec comme contenu la ligne suivante `IP_PRIMARY:5432:replication:USER_REPLICATOR:PASSWORD_REPLICATOR` (remplacez les variables majuscules par des variables de template).
      * Vous pouvez ajouter des variables disponibles uniquement pour cette tâche sous la clé `vars`.
      * Le fichier devra appartenir à l'utilisateur et groupe `postgres` et posséder les accès `0600`.
  1. Arrêter le service `postgresql`.
  1. Vider le répertoire des données PostgreSQL :
      * Supprimer le répertoire. Vous récupérerez en amont son chemin avec une requête `SHOW data_directory` (avant d'arrêter le service).
      * Créer un répertoire vide au même chemin avec comme utilisateur et groupe `postgres` et droits `0700`.
  1. Effectuer la réplication via le module `command` avec
      * pour commande (remplacez les variables majuscules) :
      ```
      pg_basebackup -h IP_PRIMARY -U USER_REPLICATOR -p 5432 -D DATA_DIRECTORY -X stream
      ```
      * pour variable d'environnement : `PGPASSFILE: "/tmp/.pgpass"`
  1. Supprimer le fichier `/tmp/.pgpass` pour ne pas le laisser trainer.
  1. Via le module `lineinfile`, modifiez le fichier `/etc/postgresql/15/main/postgresql.conf` pour spécifier la source de réplication (ce paramètre ne peut pas être défini par `postgresql_set`) :
      ```
      primary_conninfo = 'host=IP_PRIMARY port=5432 user=USER_REPLICATOR password=PASSWORD_REPLICATOR'
      ```
  1. (Re)démarrer le service `postgresql`.

Faites en sorte de séparer les tâches des différents modes dans différents fichiers.

**Debug :** si la réplication ne fonctionne pas, vérifiez :

- que le `pg_hba.conf` de la VM *primary* autorise bien l'IP de la VM *standby*
- que vos VMs ont bien des règles de pare-feu GCP qui autorisent l'accès au port PostgreSQL depuis l'extérieur (voir début du TP)


## Connecter l'application au cluster PostgreSQL

Côté Kubernetes, nous allons faire en sorte que le service "worker" communique avec le nœud *primary* (écriture des données), et le service "result" avec le nœud *standby* (lecture des données).

Nous ne voulons pas modifier le code des applications pour modifier le nom de domaine utilisé pour se connecter à la base de donnée (`db`).
Pour rappel, ce nom de domaine est créé avec le nom du `Service`.

Pour le "worker", il n'y a rien à modifier.
En revanche, pour le "result", afin que le nom `db` pointe vers une autre IP, nous allons placer tout ce qui concerne "result" dans un namespace dédié que nous appellerons `db-replica`, et créer un nouveau `Service` et `EndpointSlice` dans ce namespace pointant vers l'IP de *standby*.
Ainsi nous aurons deux services nommés `db` : un dans le namespace `default`, un dans le namespace `db-replica`.
Les pods des Deployment dans le namespace par défaut (`default`) résolveront `db` vers l'IP *primary* et ceux du namespace `db-replica` vers l'IP de *standby*.

Les modifications à apporter aux manifestes Kubernetes sont donc les suivantes :
  1. Ajouter la ressource du namespace `db-replica`. Voici le manifeste :
  ```
  apiVersion: v1
  kind: Namespace
  metadata:
    name: db-replica
  ```
  2. Dupliquer le `Service` `db` et le modifier en ajoutant sous `metadata` l'attribut `namespace: db-replica`.
  3. Idem pour le `EndpointSlice`, modifier l'IP pour celle du nœud *standby*.
  4. Modifier le `Deployment` de "result" et le `Service` associé pour les placer dans le namespace `db-replica`.

Si tout est OK, le frontend `result` devrait afficher le nombre de vote.
