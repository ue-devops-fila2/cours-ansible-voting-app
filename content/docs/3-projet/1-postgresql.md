---
weight: 1
title: "Étape 1 : déployer une base de données"
---

# Étape 1 : déployer une base de données

## Infrastructure

Nous travaillerons sur une VM GCP. Commencez par créer une instance via le tableau de bord.
  * Choisissez les mêmes Région et une Zone que votre cluster GKE.
  * Vérifiez que vous pouvez vous connecter en SSH (changez le nom de l'instance):
    ```
    gcloud compute ssh instance-20240430-95bdf
    ```


## Déployer PostgreSQL grâce à Ansible

### Structure de base

Forkez et clonez le dépôt <https://gitlab.imt-atlantique.fr/ue-devops-fila2/tp-ansible-voting-app> si ce n'est pas déjà fait.

La première partie concerne le déploiement automatisé de la base de données PostgreSQL nativement sur une VM, et non plus à travers un conteneur.
Pour cela, vous créerez un inventaire, un rôle et un playbook liant le tout.

Le playbook est situé à la racine du répertoire. Le rôle et l'inventaire sont dans des des sous-répertoires `inventories/` et `roles/` respectivement.
Voici un `tree` de ce à quoi cela pourrait ressembler.

```
├── ansible.cfg
├── deploy_postgres.yaml
├── inventories
│   └── all.yaml
└── roles
    ├── pg_install
    │   └── tasks
    │       └── main.yaml
```

### Création du rôle d'installation de PostgreSQL

Vous aurez besoin des modules suivants
  * [`apt`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html) pour installer des paquets.
  * [`service`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html) pour démarrer ou redémarrer des services systèmes.
  * [La collection de modules `postgresql`](https://docs.ansible.com/ansible/latest/collections/community/postgresql/index.html) permettant d'effectuer toutes sortes d'actions opérationnelles ou de configuration sur une base de données PostgreSQL.

Concrètement, pour installer PostgreSQL, il faut:
  1. Installer les paquets `postgresql-15` et `python3-psycopg2`. Ce dernier est un prérequis aux modules de la collection `postgresql`.
  1. Démarrer le service `postgresql@15-main`.
  1. Créer une base de données nommée `postgres`.
  1. Créer l'utilisateur `postgres` avec le mot de passe `postgres`.
  1. Accorder les droits à l'utilisateur `postgres` via le module `postgresql_privs` avec, entre autre, les champs
    `type: database` et `privs: all`
  1. Accorder le droit de connexion à l'utilisateur `postgres` via le module `postgresql_pg_hba`. Ce module modifie le fichier de configuration `pg_hba.conf` spécifié dans le champs `dest`.
      - Pour obtenir le chemin de `pg_hba.conf`, exécutez la commande suivante sur la VM:
      ```
      sudo -u postgres psql -c "SHOW hba_file;"
      ```
      * Ce fichier se configure en ajoutant des enregistrements de la forme
      ```
      CONNEXION_TYPE        DATABASE  USER  ADDRESS  METHOD  [OPTIONS]
      ```
      qui se traduit dans le module `postgresql_pg_hba` par les champs `contype`, `databases`, `users`, `address` et `method`. Utilisez `host` pour `contype`, `all` pour `address` et `md5` pour `method`.
  1. Définir l'adresse d'écoute du server sur toutes les interfaces. Le module `postgresql_set` permet de modifier des valeurs dans un autre fichier de configuration: `postgresql.conf`. Il n'est pas nécessaire de spécifier son chemin. Affectez l'attribut `listen_addresses` à `0.0.0.0`.
  1. Enfin, redémarrer le service `postgresql`.

De plus, les tâches des modules `postgresql` doivent s'exécuter avec l'utilisateur système `postgres`, et les tâches d'installation de paquets et (re)démarrage du service avec l'utilisateur `root`.


### Tester le bon fonctionnement

Afin que la base de données soit accessible publiquement, il faut autoriser les connexions sur le port 5432 de la VM à travers une *règle de pare-feu*.
Sur GCP, les règle de pare-feu fonctionnent un peu comme les "selector" dans Kubernetes.
Les VM ont des *tags* qui leur sont associés, et les règle de pare-feu contiennent des tags cibles définissant à quels resources elles sont appliquées.

Vous devez donc
  1. Ajouter un tag à notre VM, par exemple le tag `db-server`. Pour cela, nous utiliserons la CLI `gcloud` (changez le nom de l'instance):
  ```
  gcloud compute instances add-tags instance-20240502-065104 --tags=db-server
  ```
  2. Ajouter une règle de pare-feu sur GCP pour autoriser les connexions entrantes sur le port 5432, avec notre tag comme cible.
      * Sur le tableau de bord GCP, cherchez le service "pare-feu".
      * Cliquer "Créer une règle de pare-feu". Choisissez un nom pour la règle.
      * Dans "Tags cibles", entrez votre tag `db-server`.
      * Dans "Plages IPV4 sources", entrez `0.0.0.0/0` pour autoriser toutes les IP sources.
      * Dans "Protocoles et ports", sélectionnez "TCP" et entrez le port `5432` de PostgreSQL.

Pour vérifier que la règle s'applique bien, dans le service "Pare-feu" de GCP, cliquez sur le nom de la règle pour voir les détails. En bas de la page, une section indique les resource auxquelles elle est appliquée, la VM devrait y apparaitre. Il se peut qu'il faille attendre un peu.


## Connecter Kubernetes à notre base PostgreSQL


Notre application tourne sur un cluster Kubernetes. Deux microservices "result" et "worker" accèdent à la base de données à travers le nom de domaine `db` associé au `Service` du même nom. Jusqu'ici, le `Service` définissait un `selector` qui liait ce service au `Deployment` associé. Or, la base de données est désormais hébergé en dehors du cluster. Pour faire communiquer le cluster vers l'extérieur, nous allons conserver notre `Service` `db` mais cette fois-ci *sans* `selector`, puis nous créons une resource `EndpointSlice` qui aura le *même nom* que le `Service` et définissant l'IP externe vers laquelle renvoyer les requêtes.

Avant de modifier vos manifestes, pensez à conserver ceux sur la partie du projet Kubernetes, par exemple en créant un répertoire à part pour cette partie.

Vous devez donc:
  1. Supprimer les manifestes liés à la base de données, sauf le `Service`.
  1. Modifier le `Service`:
      * Supprimer le champs `selector`
      * Ajouter l'attribut `protocol: TCP` à l'objet du port 5432.
  1. Ajouter la resource `EndpointSlice` dont voici le manifeste (changez l'IP)
  ```
  apiVersion: discovery.k8s.io/v1
  kind: EndpointSlice
  metadata:
    name: db
    labels:
      kubernetes.io/service-name: db
  addressType: IPv4
  ports:
    - port: 5432
  endpoints:
    - addresses:
      - "34.155.103.217"
  ```

Si tout est OK, le frontend `result` devrait afficher le nombre de votes.

Si ça ne fonctionne, voir "Pistes pour débuguer" ci-dessous.

## Refactor du rôle Ansible

1. Utilisez des variables pour le nom d'utilisateur et le mot de passe. Affectez leur valeur dans le playbook.
1. Utilisez une variable pour le champs `dest` de l'appel au module `postgresql_pg_hba`, pour cela :
    * utilisez le module `postgresql_query` pour effectuer la requête `SHOW hba_file` et enregistrez la valeur de retour,
    * sélectionnez le chemin du fichier à travers la nouvelle variable. Vous pouvez utiliser le module `debug` pour afficher la variable complète.
1. Au lieu de redémarrer le service `postgresql` explicitement dans la liste des tâches, utilisez un Handler et l'attribut `notify` quand c'est nécessaire (lors de la modification du `pg_hba.conf`).

## Pistes pour débuguer

### Tester la connectivité depuis votre machine

L'image docker `postgres` contient plusieurs commandes qui nous seront utiles pour débuguer PostgreSQL.
L'utilitaire `pg_isready` permet de vérifier que la base de donnée est bien accessible et opérationnelle.
Tapez la commande suivante (changez l'IP) depuis votre machine :
```
docker run postgres pg_isready --host=34.155.103.217
```
Si tout est OK, elle devrait renvoyer quelque chose comme
```
34.155.103.217:5432 - accepting connections
```

### Tester la connectivité de `db` depuis le cluster K8s

Vous pouvez également utiliser l'image `postgres` sur le cluster Kubernetes pour tester la connectivité **depuis le cluster** et non depuis votre machine.
Pour créer un Pod avec l'image `postgres` :

```
kubectl run postgres --image=postgres:15 -- sleep infinity
```
La partie `-- sleep infinity` remplace la commande par défaut de l'image. Le Pod ne fait donc rien mais tourne avec tous les utilitaires PostgreSQL.
Pour s'y connecter en mode interactif :

```
kubectl exec -it postgres -- bash
```

Vous pouvez maintenant utiliser par exemple `pg_isready` avec le nom de domaine `db`.


### Tester la connection de l'utilisateur postgres

Via `docker run` depuis votre machine ou bien depuis le Pod sur votre cluster, tester si l'utilisateur `postgres` arrive bien à se connecter. La commande à adapter est :

```
psql -U postgres -h 34.155.29.4 -p 5432
```


### Regarder les logs de PostgreSQL

Depuis une connexion SSH sur votre VM, vous pouvez inspecter les logs de la base de données.
Vous verrez notamment les tentatives de connexions et les messages d'erreurs associés.
Le fichier de logs se trouve dans `/var/log/`. Par exemple :

```
tail /var/log/postgresql/postgresql-15-main.log
```
